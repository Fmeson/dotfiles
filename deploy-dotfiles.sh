#!/bin/bash

# copy ssh-config to ~/.ssh/config
[[ -f ~/.ssh/config ]] || cp ~/dotfiles/ssh-config ~/.ssh/config

# hardlink nvim
if [[ "$(ls nvim/)" != "$(ls ~/.config/nvim)" ]]; then
	mkdir -p ~/.config/nvim/
	ln -f nvim/init.vim ~/.config/nvim/init.vim
	ln -f nvim/plugin.vim ~/.config/nvim/plugin.vim
fi

# hardlink $HOME/dotfiles
ln -f shell/dot-zshrc ~/.zshrc
ln -f shell/dot-bashrc ~/.bashrc
ln -f shell/dot-gitconfig ~/.gitconfig
ln -f shell/dot-inputrc ~/.inputrc
ln -f shell/dot-profile ~/.profile

# change shell to zsh
[[ ! "$ZSH_NAME" ]] || chsh -s /usr/bin/zsh "$USER"

# x11 stuff
# shell/dot-xinitrc shell/dot-Xresources

# emacs + doom-emacs
if [[ $(command -v emacs) ]]; then
	[[ -d ~/.config/doom/ ]] || cp -r doom/ ~/.config/

	if [[ -d ~/.emacs/bin/doom ]]; then
		git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
		doom install
	fi
fi

if [[ $(command -v starship) ]]; then
	if [ -z "$SSH_CLIENT" ] || [ -z "$SSH_TTY" ]; then
		cp starship_ssh.toml ~/.config/starship.toml
	else
		cp starship.toml ~/.config/starship.toml
	fi
fi

# keyboard
if [[ "$(ls keyboard/)" != "$(ls ~/.config/keyboard/)" ]]; then
	cp -r keyboard/ ~/.config/
	sh ./keyboard/install-custom-kbd.sh
fi

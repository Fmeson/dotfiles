(use-package! emacs
  :init (setq! wuphys/doom-dashboard-sound nil)
        (setq! fancy-splash-image "~/.config/doom/dashboard/doom-logo.png"
               +doom-dashboard-banner-padding '(0 . 0)
               display-buffer-alist '(("\\*Messages\\*"
                                       display-buffer-no-window)))
          ;;(add-hook! +doom-dashboard-mode :append
          ;;   (when (wuphys/doom-dashboard-sound)
          ;;         (async-shell-command "play ~/.config/doom/dashboard/E1M1-cut.mp3"
          ;;                              "\*Messages\*" nil)))
  :config (set-frame-font "Fantasque Sans Mono 16" nil t)
          (fset 'yes-or-no-p 'y-or-n-p)
          (setq! display-line-numbers-type nil
                 auto-revert-verbose t
                 global-auto-revert-mode t
                 auto-revert-use-notify nil
                 doom-theme 'doom-solarized-dark
                 vc-follow-symlinks t
                 writeroom-fullscreen-effect t
                 global-whitespace-mode t
                 whitespace-mode t))
(setq! org-directory "~/notes/")
;;(add-hook! org-mode (org-superstar-restart))
;;(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
;;(use-package! org-bullets
  ;;:config
  ;;(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
(use-package! org-roam
  :hook
  (after-init . org-roam-mode)
  :config ;;(global-prettify-symbols-mode)
		  ;;(global-prettify-symbols-mode -1)
		  ;;(setq! global-prettify-symbols-mode -1)
  		  (setq! org-roam-buffer-position 'right
                 org-roam-buffer-width 35
                 org-roam-directory "~/notes/"
                 ;;org-roam-dailies-directory "daily/"
                 org-roam-dailies-capture-templates
                 '(("d" "default" entry
                    #'org-roam-capture--get-point
                    "* %?"
                    :file-name "daily/%<%Y-%m-%d>"
                    :head "#+title: %<%Y-%m-%d>\n\n"))))

(use-package! doom-modeline
  :config (setq! doom-modeline-minor-modes nil
                 doom-modeline-buffer-encoding nil
                 doom-modeline-icon (display-graphic-p)
                 doom-modeline-major-mode-icon t))

(use-package! org
  :init (setq! org-directory "~/notes/"
               +org-capture-todo-file     "tasks.org"
               +org-capture-projects-file "projects.org"
               +org-capture-notes-file    "notes.org")
        ;;(setq! prettify-symbols--keywords nil)
        (setq! org-noter-always-create-frame nil
               org-startup-folded t
               org-startup-with-latex-preview 1
               org-startup-with-inline-images 1
               org-use-speed-commands t
               org-log-into-drawer t
               org-log-done t
               org-directory "~/notes/")
  :config (setq! org-superstar-leading-bullet ?\s
                 org-superstar-leading-fallback ?\s
                 org-hide-leading-stars t
                 org-superstar-todo-bullet-alist
                 '(("TODO" . 9744)
                   ("[ ]"  . 9744)
                   ("DONE" . 9745)
                   ("[X]"  . 9744)))
  (setq! org-src-window-setup "current-window")
  (setq! org-todo-keywords
         '((sequence  " KILL(k)" " DONE(d)")
           (sequence "(t)" "|" "(D)")
           (type " TASK(t)" " APPT(a)" "|")
           (type " CNFG(c)" " IDEA(i)" "襁 FIX!(f)" "|")
           (type " STDY(s)" " LKUP(l)" " RSRC(r)" "|")
           (type " READ(R)" " LISN(L)" " WTCH(W)" "|")))

  (setq! org-todo-keyword-faces
         '((""      . org-agenda-dimmed-todo-face)
           (" KILL" . org-agenda-dimmed-todo-face)
           (" DONE" . org-agenda-dimmed-todo-face)
           (""      . org-todo)
           (" PROJ" . message-header-subject)
           (" APPT" . message-header-subject)
           (" TASK" . message-header-subject)
           (" TODO" . message-header-subject)
           (" CNFG" . +org-todo-active)
           (" LKUP" . +org-todo-project)
           (" STDY" . +org-todo-project)
           (" RSRC" . +org-todo-project)
           (" READ" . org-agenda-date)
           (" WTCH" . org-agenda-date)
           (" LISN" . org-agenda-date)
           ("襁 FIX!" . magit-section-heading-selection)
           (" IDEA" . doom-modeline-evil-visual-state)
           (" LKUP" . +org-todo-project)))

  (map! :map evil-org-agenda-mode-map
        :m "<return>"  #'wuphys/org-agenda-refile-closed-item
           "<escape>"  #'org-agenda-quit)
  (add-hook! org-mode
             ;;(rainbow-delimiters-mode)
             (setq! org-hidden-keywords '(title))
             (spell-fu-mode -1)
             (set-face-attribute 'org-document-title nil :height 200)
             (set-face-attribute 'org-block nil :background)
             (set-face-attribute 'org-drawer nil :foreground "#626C6C")
             (set-face-attribute 'org-block-begin-line nil :background)
             (set-face-attribute 'org-block-end-line nil :background)))

(defun wuphys/org-agenda-change-view (rotation)
  "Rotate agenda list to next view and display"
  (let* ((agenda-view-list
          (-rotate rotation wuphys/org-agendas-list))
         (file (car (car agenda-view-list)))
         (custom-view (cdr (car agenda-view-list))))
    (setq! org-agenda-files `(,file)
           wuphys/org-agendas-list agenda-view-list)
    (org-agenda nil custom-view)))

(defun wuphys/org-agenda-next-view ()
  (interactive)
  "Go to next org agenda view"
  (wuphys/org-agenda-change-view +1))

(defun wuphys/org-agenda-prev-view ()
  (interactive)
  "Go to prev org agenda view"
  (wuphys/org-agenda-change-view -1))

(add-hook! +doom-dashboard-mode
(setq! org-agenda-time-leading-zero t
       org-agenda-show-inherited-tags "always"
       org-agenda-timegrid-use-ampm t
       org-agenda-show-current-time-in-grid t
       org-agenda-block-separator nil
       org-agenda-hide-tags-regexp nil)

;; Save edits to files when quitting org-agenda-mode
(advice-add 'org-agenda-quit :before 'org-save-all-org-buffers)
;;
;; Set initial org agenda view to be first in wuphys/org-agendas-list
(setq! wuphys/org-agendas-list
       '(("~/notes/tasks.org" . "g")
         ("~/.config/doom/config.org" . "g")
         ("~/notes/linux/linux.org". "g")))
;;(setq! org-agenda-files (car (car wuphys/org-agendas-list)))
(setq! org-agenda-files  `(,(car (car wuphys/org-agendas-list))))
(setq! org-agenda-use-time-grid t
       org-agenda-window-setup "other-tab"
       ;; inhibit-frame-set-background-mode t
       ;; persp-init-new-frame-behaviour-override t
       org-agenda-current-time-string
       "Now -·-·-·-·-·-·-"
       org-agenda-time-grid
       '((daily today weekly)
         (600 800  1000 1200
              1400 1600 1800)
         " ......." "-----------------")
       org-agenda-remove-times-when-in-prefix nil
       org-agenda-prefix-format
       '((agenda    . " %i %?-20t% s")
         (todo      . "  ")
         (tags      . "  ")
         (search    . "  ")))

(setq! org-agenda-custom-commands
       '(("g" "Gitter Done!"
          ((tags "+LEVEL>1+CATEGORY=\"Inbox\""
                 ((org-agenda-overriding-header "<==Inbox==>")
                  (org-agenda-show-tags)))
           (tags "+LEVEL>1+CATEGORY=\"Active\""
                 ((org-agenda-overriding-header "\n<==Active==>")
                  (org-agenda-show-tags)))
           (tags "CLOSED>=\"<today>\""
                 ((org-agenda-overriding-header "\n<==Finished Today==>")
                  (org-agenda-show-tags)))
           (agenda "daily"
                   ((org-agenda-span 'day)
                    (org-deadline-warning-days 0)
                    (org-agenda-overriding-header "\n<==Today==>")))))))

(add-hook! org-agenda-finalize
  (map! :map evil-org-agenda-mode-map
        :mn "C-j"  #'wuphys/org-agenda-next-view
        :mn "C-k"  #'wuphys/org-agenda-prev-view)
  (+word-wrap-mode)
  (evil-goto-first-line)
  (setq org-agenda-start-day "+0")
  (org-agenda-goto-today)))

(defun wuphys/refile (file headline &optional state)
  (interactive)
  (let ((pos
         (save-excursion (org-find-exact-headline-in-buffer headline
                           (find-file-noselect file)))))
    (unless (ignore-errors (org-agenda-todo state))
    (org-agenda-refile nil (list headline file nil pos) t)))
  (switch-to-buffer (current-buffer) nil t))

(defun wuphys/org-agenda-refile-closed-item ()
  (interactive)
       (wuphys/refile (car org-agenda-files) "Closed Tasks" "DONE")
       (org-agenda-redo))

(defun wuphys/org-agenda-refile-to-next-tasks ()
  (interactive)
       (wuphys/refile (car org-agenda-files) "Tasks" "NEXT")
       (org-agenda-redo))

;;(use-package! org-fancy-priorities
;;  :requires (org org-roam)
;;  :hook (org-agenda-mode . org-fancy-priorities-mode)
;;  :config (setq org-fancy-priorities-list '("!" "^" "_")))

;;("t" " TODO(t)" entry
;;           (file+headline +org-capture-todo-file "Inbox")
;;           ,(concat "*  TODO %?\n"
;;                    ":PROPERTIES:\n:CAPTURED:%U\n:END:") :prepend t)
;;
;;          ("p" " PROJ(p)" entry
;;           (file+headline +org-capture-notes-file "Inbox")
;;           "*  PROJ %u %?\n%i\n%a" :prepend t)
;;(add-hook! emacs-startup-hook
  (setq! org-capture-templates
        `(("f" "襁 FIX!(f)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "* 襁 FIX! %? :fix:\n"
                    ":PROPERTIES:\n:CAPTURED:%U\n:END:") :prepend t)
          ("l" " LKUP(l)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "*  LKUP %?\n"
                    ":PROPERTIES:\n:CAPTURED:%U\n:END:") :prepend t)
          ("T" " TASK(t)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "*  TASK%?\n"
                    ":PROPERTIES:\n:CAPTURED:%U\n:END:") :prepend t)
          ("i" " Idea(i)" entry
           (file+headline +org-capture-todo-file "Inbox")
           ,(concat "*  IDEA %? \n"
                    ":PROPERTIES:\nCAPTURED:%U\n:END:") :prepend t)
          ("a" " APPT(a)" entry
           (file+headline +org-capture-todo-file "Scheduled")
           ,(concat "*   APPT %?\n"
                    "SCHEDULED: %^t\n:PROPERTIES:\n:CAPTURED: %U\n:END:\n")
             :prepend t)))

(use-package! org-roam
  :requires org
  :config (setq! org-roam-buffer-position 'right
                 org-roam-buffer-width .33
                 org-roam-directory "~/notes/")
          (setq! org-roam-capture-templates
                 '(("d" "default" plain (function org-roam--capture-get-point)
                      "%?"
                      :file-name "${slug}"
                      :head "#+title:${title}\n"
                      :unnarrowed t)))
          (org-roam-mode 1))

;;(use-package! org-download
;;  :after org
;;  :bind
;;  (:map org-mode-map
;;   (("s-Y" . org-download-screenshot)
;;    ("s-y" . org-download-yank))))

(set-popup-rule! "^\\*Capture\\*"
                      :side     'bottom
                      :size     .25
                      :height   .25
                      :width    .40
                      :modeline nil
                      :quit     t)
(set-popup-rule! "^\\*Org Agenda"
                      :side     'right
                      :size     .35
                      :width    .35
                      :modeline nil
                      :quit     t)

(defun evil-command-window-ex ()
            (interactive)
            (evil-ex))

(use-package! evil
  :config (setq! evil-echo-state nil
                 evil-split-window-below t
                 evil-vsplit-window-right t
                 evil-mode-line-format nil))

(add-hook! +doom-dashboard-mode-hook
 (map! :map evil-motion-state-map
       "C-f"          #'dired
       "C-e"  #'evil-end-of-line))

(map! "C-c c"        #'org-capture
      "C-c a"        (cmd! (org-agenda nil "g"))
      "C-c C-;"      #'iedit-mode
      "C-c t"        #'org-todo
      "C-a"          #'evil-first-non-blank
      "C-q"          #'kill-this-buffer
      "C-;"          #'counsel-M-x
      "C-h RET"      #'man
      "C-h i"        #'info-other-window
      "M-<return>"   #'+vterm/toggle
      "H-s-<return>"   #'+eshell/toggle
      "M-s-<return>"   #'+eshell/toggle
      "C-:"          #'ivy-resume
      "C-/"          #'swiper
      "C-s"          #'save-buffer
      :leader
      (:prefix ("t" . "toggle")
        "c" 'centered-cursor-mode
        "R" 'rainbow-delimiters-mode
        "W" 'whitespace-mode)
      (:prefix ("g" . "git")
        "n" 'git-gutter:next-hunk
        "p"  'git-gutter:prev-hunk)
      (:prefix ("s" . "search")
        "R"  'counsel-mark-ring
        "h"  'counsel-imenu
        "r"  'rg)
      (:prefix ("n" . "notes")
        "c" 'org-capture
        "R" 'refile
        "W" 'org-agenda
        "a" (cmd! (org-agenda nil "g"))
        "n" 'org-roam-capture
        "t" 'org-capture
        "T" 'org-capture))

(after! (evil-org org evil)
 (map! :map (evil-motion-state-map evil-org-mode-map)
       "C-j"  (cmd! (org-next-visible-heading +1))
       "C-k"  (cmd! (org-next-visible-heading -1))
       "C-f"  #'dired
       "C-a"  #'evil-first-non-blank
       "C-e"  #'evil-end-of-line)

 (map! :map (evil-motion-state-map evil-org-mode-map org-mode-map)
       :mn "C-j"  (cmd! (org-next-visible-heading +1))
       :mn "C-k"  (cmd! (org-next-visible-heading -1))))

;;
;;(map! :leader
;;(:prefix ("TAB" . "workspace")
;;  "1" #'wuphys/deer-side-panel-open
;;  "2" #'wuphys/ibuffer-side-panel-open
;;  "3" #'+treemacs/toggle)))

(setq-default evil-cross-lines t)

(defun evil-next-line--check-visual-line-mode (orig-fun &rest args)
  (if visual-line-mode
      (apply 'evil-next-visual-line args)
    (apply orig-fun args)))

(advice-add 'evil-next-line :around
            'evil-next-line--check-visual-line-mode)

(defun evil-previous-line--check-visual-line-mode (orig-fun &rest args)
  (if visual-line-mode
      (apply 'evil-previous-visual-line args)
    (apply orig-fun args)))

(advice-add 'evil-previous-line :around
            'evil-previous-line--check-visual-line-mode)

;;(add-hook! emacs-lisp-mode (prettify-symbols-mode -1))
(use-package! lispy
  :hook emacs-lisp-mode
  :config
    (add-hook! 'doom-scratch-buffer-hook (lambda () (lispy-mode 1))))

(use-package! magit
  :config (map! :map magit-mode-map
            :m "<tab>" #'magit-section-toggle
            :m "TAB"   #'magit-section-toggle
            :n "<tab>" #'magit-section-toggle
            :n "TAB"   #'magit-section-toggle)
          (set-popup-rule! "^magit:"
                             :side     'right
                             :size     .35
                             :width    .35
                             :modeline nil
                             :quit     t))

(use-package! all-the-icons-ibuffer
  :config
    (setq ibuffer-formats
            `((mark " " (icon 2 2 :left :elide)
                      ,(propertize "" 'display `(space :align-to 8))
                      (name 30 50 :left :elide)
                      " " filename-and-process+)
                (mark " " (name 20 -1) " " filename))))

;;(set-popup-rule! "^\\*\\(helpful\\|info\\|Man\\|Help\\)"
;;                      :side     'right
;;                      :size     .50
;;                      :width    .50
;;                      :modeline nil
;;                      :quit     t)))
(setq Man-notify-method 'aggressive)
(add-hook! (helpful-mode Man-mode Info-mode) centered-cursor-mode)

(setq default-alias-fallback "everything")

(setq treemacs-show-hidden-files nil)

(setq which-key-idle-delay 0.3)

(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)

(setq define-it-show-dictionary-definition t)
(setq define-it-show-wiki-summary t)
(setq define-it-show-google-translate nil)
(setq define-it-output-choice 'view)

(use-package company
  :defer 2
  :diminish
  :custom
  (company-begin-commands '(self-insert-command))
  (company-idle-delay .1)
  (company-minimum-prefix-length 2)
  (company-show-numbers t)
  (company-tooltip-align-annotations 't)
  (global-company-mode t))

(use-package company-box
  :after company
  :diminish
  :hook (company-mode . company-box-mode))
(setq company-backends '(company-async-files))

(map! :map company-active-map
      "TAB" #'company-complete)

(setq ispell-alternate-dictionary "en")
;;(defun org-keyword-backend (command &optional arg &rest ignored)
;;  (interactive (list 'interactive))
;;  (cl-case command
;;    (interactive (company-begin-backend 'org-keyword-backend))
;;    (prefix (and (eq major-mode 'org-mode)
;;                 (cons (company-grab-line "^#\\+\\(\\w*\\)" 1)
;;                       t)))
;;    (candidates (mapcar #'upcase
;;                        (cl-remove-if-not
;;                         (lambda (c) (string-prefix-p arg c))
;;                         (pcomplete-completions))))
;;    (ignore-case t)
;;    (duplicates t)))
;;
;;(add-to-list 'company-backends 'org-keyword-backend)

(setq vterm-term-environment-variable "eterm-color")
(set-popup-rule! "^\\*doom:vterm"
                      :side     'bottom
                      :size     .35
                      :height   .35
                      :width    .40
                      :modeline nil
                      :quit     t)
(set-popup-rule! "^\\*doom:eshell"
                      :side     'bottom
                      :size     .35
                      :height   .35
                      :width    .40
                      :modeline nil
                      :quit     t)
;;(add-hook! vterm-mode
;;		  (global-prettify-symbols-mode -1))

(setq elfeed-feeds
      '(("https://feeds.twit.tv/floss.xml" tech-news twit freesoftware podcast)
        ("https://anchor.fm/s/11047e1c/podcast/rss" podcast mensa)
        ("https://sfconservancy.org/feeds/omnibus/" activism freesoftware)
        ("https://allisonrandal.com/feed/" freesoftware blog)
        ("https://feeds.twit.tv/sn.xml" tech-news twit security podcast)
        ("https://www.opensourcecreative.org/episode/index.xml" creative podcast)
        ("https://feeds.twit.tv/twig.xml" tech-news twit google podcast)
        ("https://feeds.twit.tv/tnw.xml" tech-news twit podcast)
        ("https://changelog.com/podcast/feed" tech-news software podcast)
        ("https://changelog.com/brainscience/feed" science podcast)
        ("https://twit.tv/shows/ham-nation" tech-news twit ham podcast)
        ("https://feeds.twit.tv/twit.xml" tech-news twit podcast)
        ("http://feeds.propublica.org/propublica/main" news independent)
        ("https://hackaday.com/blog/feed/" hackaday tech)
        ("https://static.fsf.org/fsforg/rss/news.xml" activism freesoftware)
        ("https://www.archlinux.org/feeds/news/" linux distro)
        ("https://protesilaos.com/codelog.xml" linux prot individual)
        ("https://protesilaos.com/news.xml" prot news individual)
        ("https://bitwarden.com/blog/feed.xml" company security)
        ("https://stallman.org/rss/rss.xml" rms activism freesoftware individual)
        ("https://protesilaos.com/politics.xml" politics prot individual)
        ("https://planet.emacslife.com/atom.xml" emacs community)
        ("http://techrights.org/feed/" freesoftware)
        ("https://archive.org/iathreads/posts-display-new.php?forum=news&mode=rss" ngo library news)
        ("https://about.gitlab.com/atom.xml" company gitlab)
        ("https://www.eff.org/rss/updates.xml" activism privacy)))

(setq user-mail-address "wuphysics87@protonmail.com"
      user-full-name  "Sean Wu")

(setq mu4e-compose-signature "-Sean")

(setq mu4e-maildir-shortcuts
        '((:maildir "/arXiv/"  :key ?a)
          (:maildir "/inbox/"  :key ?i)
          (:maildir "/drafts/" :key ?d)
          (:maildir "/trash/"  :key ?t)
          (:maildir "/sent/"   :key ?s)))

;; Send mail
(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-auth-credentials "~/.authinfo.gpg"
      smtpmail-smtp-server "127.0.0.1"
      smtpmail-stream-type 'starttls
      smtpmail-smtp-service 1025)

(setq mu4e-maildir "~/.mail"
	  mu4e-attachment-dir "/attachments/"
	  mu4e-sent-folder    "/sent/"
	  mu4e-drafts-folder  "/drafts/"
	  mu4e-trash-folder   "/trash/"
	  mu4e-refile-folder  "/arXiv/")

;; Get mail
(setq mu4e-get-mail-command "mbsync protonmail"
	  mu4e-change-filenames-when-moving t   ; needed for mbsync
	  mu4e-update-interval 120)             ; update every 2 minutes

(setq eww-search-prefix "https://duckduckgo.com/html/?q=")
(setq eww-download-directory "~/downloads/")
(setq eww-restore-desktop nil)
(setq shr-use-fonts nil)
(setq shr-use-colors nil)

(add-hook! pdf-view-mode (pdf-view-midnight-minor-mode 1))

(setq! deft-extensions '("org")
       deft-recursive t
       deft-directory "~/notes/")

(use-package! ansible-doc
  :hook yaml-mode
  :config (evil-set-initial-state 'ansible-doc-module-mode 'motion)
          (evil-set-initial-state 'ansible-doc-mode 'normal)
          (add-hook! ansible-doc-module-mode centered-cursor-mode))
;;(add-hook! ansible-doc-module-mode (prettify-symbols-mode -1))
;;(add-hook! ansible-doc-mode (prettify-symbols-mode -1))
;;(add-hook! yaml-mode (prettify-symbols-mode -1))
;;(add-hook! yaml-mode (spell-fu-mode -1))

(use-package! ansible
  :hook yaml-mode
  :config ;;(prettify-symbols-mode -1)
           (spell-fu-mode -1))

  ;;:config (add-hook 'yaml-mode-hook (lambda ()(ansible 1))))

;;(add-hook! +doom-dashboard-mode-hook
          ;;(add-hook! +doom-dashboard-mode :append

;;(flycheck-define-checker ansible-lint
;;  "Ansible-lint for flycheck. requires ansible-lint.
;;
;;        Install with pip install ansible-lint.
;;
;;Find on GitHub.
;;`https://github.com/ansible-community/ansible-lint'."
;;  :command ("ansible-lint" "--noout" source)
;;  :error-patterns
;;  ((error line-start (file-name) ":" line ": " (message) line-end))
;;  :modes ansible-doc-mode)

(setq! persp-save-dir "/home/sean/.emacs.d/persp-confs/")

;;(setq gc-cons-threshold 80000000)

#!/bin/sh

## When I use these I am usually in the terminal already.
## Could be put in nvim config

## starship config needs difference between local and remote

cp ~/.bashrc      ~/.config/shell/dot-bashrc
cp ~/.gitconfig   ~/.config/shell/dot-gitconfig
cp ~/.inputrc     ~/.config/shell/dot-inputrc
cp ~/.profile     ~/.config/shell/dot-profile
cp ~/.tmux.conf   ~/.config/shell/dot-tmux.conf
cp ~/.xinitrc     ~/.config/shell/dot-xinitrc
cp ~/.zshrc  	  ~/.config/shell/dot-zshrc
cp -r ~/bin/* 	  ~/.config/bin/

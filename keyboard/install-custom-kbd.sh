#!/bin/bash

## Install tty config
# sudo rm /usr/share/kbd/keymaps/custom.map
# sudo rm /etc/vconsole.conf
sudo cp ~/.config/keyboard/tty-custom.map  /usr/share/kbd/keymaps/custom.map
echo "KEYMAP=/usr/share/kbd/keymaps/custom.map" | sudo tee -a /etc/vconsole.conf

## Increase repeat on hold speed
# sudo rm /etc/systemd/system/kbdrate.service
sudo cp ~/.config/keyboard/kbdrate.service /etc/systemd/system/kbdrate.service
sudo systemctl enable kbdrate

## Install xkb config
if diff /usr/share/X11/xkb/symbols/us.bak ~/.config/keyboard/xkb_us_vim; then
	sudo cp /usr/share/X11/xkb/symbols/us /usr/share/X11/xkb/symbols/us.bak
fi
sudo cp ~/.config/keyboard/xkb_us_vim /usr/share/X11/xkb/symbols/us
